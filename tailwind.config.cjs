const colors = require("tailwindcss-material/colors")

const config = {
  mode: "jit",
  purge: [
    "./src/**/*.{html,js,svelte,ts}",
  ],
  theme: {
    extend: {
      transitionProperty: {
        'width': 'width'
      },
      zIndex: {
        '-1': '-1',
      },
      animation: {
        'fade-in': 'fade-in 1s ease-in',
      },
      keyframes: {
        'fade-in': {
          '0%': { opacity: '0' },
          '100%': { opacity: '1' },
        }
      },
      colors: {
        ...colors,
        gray: {
          ...colors.gray,
          '950': 'rgb(26, 27, 28)',
        },
        yellow: {
          ...colors.yellow,
          '950': 'hsl(28,90%,40%)',
        },
      },
    },
  },
  plugins: [],
  darkMode: 'media',
};

module.exports = config;